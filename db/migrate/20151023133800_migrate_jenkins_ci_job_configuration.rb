class MigrateJenkinsCiJobConfiguration < ActiveRecord::Migration
  def change
    JenkinsService.all.each do |jenkins_service|
      if jenkins_service.properties.key? 'multiproject_enabled'
        jenkins_service.properties['job_configuration'] = 'multi_project'
        jenkins_service.properties.delete('multiproject_enabled')
      else
        jenkins_service.properties['job_configuration'] = 'simple'
      end
    end
  end
end